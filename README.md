# Distcrawl

Distcrawl is a distributed crawler that relies on AWS services to scale and distribute the service.

Whenever a new worker is added into the cluster, it creates a new virtual machine that copies the repository and access randomly all the queues.

At every step, there are four main points:
+ Fetch the url
+ Retrieve all the urls
+ Apply the filters to filter the invalid urls
+ Check which urls are not stored yet
+ Send the new urls to queue and storage

The use of the storage and the queue are done asyncronously. Besides, there exits an internal cache.

Command Line
----

List all the queues
```sh
$ crawler queues list
```
Create a new queue
```sh
$ crawler queues add [name optional]
```
List all the workers
```sh
$ crawler workers list
```
Create a new worker
```sh
$ crawler workers add [id optional]
```
Retrieve the urls of a specific endpoint (used for test purposes)
```sh
$ crawler parse --file [from a file] --url [from a url]
```
License
----

MIT
