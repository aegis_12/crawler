package cache

import "github.com/boltdb/bolt"

type BoltDBCache struct {
	db *bolt.DB
}

const bucketName = "cache"

func NewBoltDBCache(pathname string) (Cache, error) {
	db, err := bolt.Open(pathname, 0600, nil)
	if err != nil {
		return nil, err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		return err
	})
	if err != nil {
		return nil, err
	}

	return &BoltDBCache{
		db: db,
	}, nil
}

func (r *BoltDBCache) Get(u string) (bool, error) {

	exists := false
	r.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		v := b.Get([]byte(u))

		exists = v == nil
		return nil
	})

	return exists, nil
}

func (r *BoltDBCache) Set(u string) error {
	return r.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		err := b.Put([]byte(u), []byte("found"))
		return err
	})
}

func (r *BoltDBCache) Close() error {
	return r.db.Close()
}
