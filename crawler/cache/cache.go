package cache

type Cache interface {
	Get(string) (bool, error)
	Set(string) error
	Close() error
}
