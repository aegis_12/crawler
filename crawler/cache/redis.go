package cache

import (
	"fmt"

	"github.com/go-redis/redis"
)

type RedisCache struct {
	client *redis.Client
}

func NewRedisCache(hostname string) (Cache, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     hostname,
		Password: "",
		DB:       0,
	})

	pong, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	if pong != "PONG" {
		return nil, fmt.Errorf("RedisCache: ping result should be 'PONG' but found '%s'", pong)
	}

	return &RedisCache{
		client: client,
	}, nil
}

func (r *RedisCache) Get(u string) (bool, error) {
	_, err := r.client.Get(u).Result()
	if err == redis.Nil {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func (r *RedisCache) Set(u string) error {
	return r.client.Set(u, "true", 0).Err()
}

func (r *RedisCache) Close() error {
	return nil
}
