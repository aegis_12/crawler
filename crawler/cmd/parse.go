package cmd

import (
	"crawler/crawler"
	"crawler/crawler/net"
	"crawler/crawler/parser"
	"fmt"
	"io/ioutil"

	"github.com/spf13/cobra"
)

var parseFile string
var parseFileDomain string
var parseRawURL string

func init() {
	RootCmd.AddCommand(parseCmd)

	parseCmd.Flags().StringVarP(&parseFile, "file", "f", "", "File")
	parseCmd.Flags().StringVarP(&parseRawURL, "url", "u", "", "Url from extract")
	parseCmd.Flags().StringVarP(&parseFileDomain, "domain", "d", "http://www.google.com", "Domain for the file")
}

var parseCmd = &cobra.Command{
	Use:   "parse",
	Short: "Parse a url",
	Run: func(cmd *cobra.Command, args []string) {
		err := parse()
		if err != nil {
			fmt.Println(err)
		}
	},
}

func parse() error {
	if parseFile != "" && parseRawURL != "" {
		return fmt.Errorf("Both url and file selected")
	}

	var f func() ([]byte, string, error)
	if parseRawURL != "" {
		f = contentFromURL
	} else if parseFile != "" {
		f = contentFromFile
	} else {
		return fmt.Errorf("Both url and file empty")
	}

	content, baseurl, err := f()
	if err != nil {
		return err
	}

	u, err := net.NewURL(baseurl)
	if err != nil {
		return err
	}

	extr := parser.EmptyParser()
	extr.ExtractUrls(u, string(content))

	return nil
}

func contentFromURL() ([]byte, string, error) {
	content, err := net.NewFetch(crawler.DefaultConfig().UserAgent).Get(parseRawURL)
	return content, parseRawURL, err
}

func contentFromFile() ([]byte, string, error) {
	content, err := ioutil.ReadFile(parseFile)
	return content, parseFileDomain, err
}
