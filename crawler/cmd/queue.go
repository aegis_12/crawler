package cmd

import (
	"crypto/rand"
	"fmt"
	"log"
	"os"

	"crawler/crawler/queue"

	"github.com/spf13/cobra"
)

func GenerateUUID() string {
	buf := make([]byte, 16)
	if _, err := rand.Read(buf); err != nil {
		panic(fmt.Errorf("failed to read random bytes: %v", err))
	}

	return fmt.Sprintf("%08x-%04x-%04x-%04x-%12x",
		buf[0:4],
		buf[4:6],
		buf[6:8],
		buf[8:10],
		buf[10:16])
}

func init() {
	queueCmd.AddCommand(listQueuesCmd)
	queueCmd.AddCommand(addQueueCmd)
}

func createQueueService() (queue.Queue, error) {
	sess := defaultAWSSession()
	logger := log.New(os.Stderr, fmt.Sprintf("[%s] ", "queues"), log.LstdFlags)

	return queue.NewSQSQueue(logger, sess)
}

var queueCmd = &cobra.Command{
	Use:   "queues",
	Short: "Print information about queues",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hugo Static Site Generator v0.9 -- HEAD")
	},
}

var addQueueCmd = &cobra.Command{
	Use:   "add [name]",
	Short: "Add a new queue",
	Run: func(cmd *cobra.Command, args []string) {

		var name string
		if len(args) == 0 {
			fmt.Println("No name found on args. Generated randomly")
			name = GenerateUUID()
		} else if len(args) == 1 {
			name = args[0]
		} else {
			fmt.Println("More than one value on args.")
			return
		}

		fmt.Println("Queue name " + name)
		queueSvc, err := createQueueService()
		if err != nil {
			panic(err)
		}

		err = queueSvc.CreateQueue(name)
		if err != nil {
			panic(err)
		}
	},
}

var listQueuesCmd = &cobra.Command{
	Use:   "list",
	Short: "List all the queues",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("list all queues")

		queueSvc, err := createQueueService()
		if err != nil {
			panic(err)
		}

		queueNames := queueSvc.ListQueues()
		fmt.Println(queueNames)
	},
}

var sendQueuesCmd = &cobra.Command{
	Use:   "send",
	Short: "Send url to queues",
	Run: func(cmd *cobra.Command, args []string) {

		/*
			queueSvc, err := createQueueService()
			if err != nil {
				panic(err)
			}
		*/

	},
}
