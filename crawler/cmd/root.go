package cmd

import (
	"crawler/crawler/storage"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(queueCmd)
}

func defaultAWSSession() *session.Session {
	return session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
}

var RootCmd = &cobra.Command{
	Use:   "crawler",
	Short: "Crawler is a very fast static site generator",
	Long:  `A Fast and Flexible Static Site Generator`,
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		run()
	},
}

func run() {
	fmt.Println("- run -")

	sess := defaultAWSSession()
	logger := log.New(os.Stderr, fmt.Sprintf("[%s] ", "queues"), log.LstdFlags)

	storage, err := storage.NewDynamoDBStorage(logger, sess)
	if err != nil {
		panic(err)
	}

	fmt.Println(storage)
}
