package net

import (
	"io/ioutil"
	"net/http"
)

type Fetch struct {
	UserAgent string
}

func NewFetch(UserAgent string) *Fetch {
	return &Fetch{
		UserAgent: UserAgent,
	}
}

func (f *Fetch) Get(u string) ([]byte, error) {

	client := &http.Client{}

	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		return []byte{}, err
	}

	req.Header.Set("User-Agent", f.UserAgent)

	resp, err := client.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}

	return data, nil
}
