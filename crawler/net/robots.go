package net

import "github.com/temoto/robotstxt"

type Robots struct {
	UserAgent string
	robots    map[string]*robotstxt.RobotsData
}

func NewRobots(UserAgent string) *Robots {
	return &Robots{
		UserAgent: UserAgent,
		robots:    map[string]*robotstxt.RobotsData{},
	}
}

func getRobots(domain string) (*robotstxt.RobotsData, error) {
	fn := Fetch{}
	data, err := fn.Get(domain + "/robots.txt")
	if err != nil {
		return nil, err
	}

	robots, err := robotstxt.FromBytes(data)
	if err != nil {
		return nil, err
	}

	return robots, nil
}

func (r *Robots) Check(domain string, path string) (bool, error) {
	robot, ok := r.robots[domain]
	if !ok {
		var err error
		robot, err = getRobots(domain)
		if err != nil {
			return false, err
		}

		r.robots[domain] = robot
	}

	return robot.TestAgent("/", r.UserAgent), nil
}
