package net

import (
	"net/url"
)

type URL struct {
	u *url.URL
}

func NewURL(rawurl string) (*URL, error) {
	u, err := url.Parse(rawurl)
	if err != nil {
		return nil, err
	}

	return &URL{u: u}, nil
}

func NewURLFromDomain(domain *URL, query string) (*URL, error) {
	rawurl := domain.u.Scheme + "://" + domain.u.Host + query
	return NewURL(rawurl)
}

func (u *URL) String() string {
	return u.u.String()
}

func (u *URL) Domain() string {
	return u.u.Host
}
