package parser

import (
	"crawler/crawler/net"
)

type filter func(baseUrl, url *net.URL) bool

type Filters []filter

func (f *Filters) Filter(baseUrl, url *net.URL) bool {
	for _, i := range *f {
		if !i(baseUrl, url) {
			return false
		}
	}

	return true
}

func DefaultFilters() Filters {
	return Filters{
		sameDomainFilter,
	}
}

func sameDomainFilter(baseUrl, url *net.URL) bool {
	return baseUrl.Domain() == url.Domain()
}
