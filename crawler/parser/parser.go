package parser

import (
	"bytes"
	"crawler/crawler/net"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type Parser struct {
	Filters Filters
}

func EmptyParser() *Parser {
	return &Parser{
		Filters: DefaultFilters(),
	}
}

func NewParser(filters Filters) *Parser {
	return &Parser{
		Filters: filters,
	}
}

func (p *Parser) ExtractUrls(baseUrl *net.URL, html []byte) ([]*net.URL, error) {
	validUrls := []*net.URL{}

	allUrls, err := p.ExtractAllUrls(baseUrl, html)
	if err != nil {
		return validUrls, err
	}

	validRepeated := map[string]*net.URL{}
	for _, url := range allUrls {
		if p.Filters.Filter(baseUrl, url) {
			validUrls = append(validUrls, url)

			if _, ok := validRepeated[url.String()]; !ok {
				validRepeated[url.String()] = url
			}
		}
	}

	for _, i := range validRepeated {
		validUrls = append(validUrls, i)
	}

	return validUrls, nil
}

func NewURL(baseurl *net.URL, rawurl string) (*net.URL, error) {
	if strings.HasPrefix(rawurl, "//") {
		return net.NewURLFromDomain(baseurl, rawurl[1:len(rawurl)])
	}

	if strings.HasPrefix(rawurl, "/") || strings.HasPrefix(rawurl, "?") {
		return net.NewURLFromDomain(baseurl, rawurl)
	}

	return net.NewURL(rawurl)
}

func checkBanned(f func(string, string) bool, s string, values []string) bool {
	for _, i := range values {
		if f(s, i) {
			return true
		}
	}

	return false
}

var Extensions = []string{
	"jpeg",
	"png",
	"jpg",
	"pdf",
	"mp3",
}

func isBannedExtension(s string) bool {
	return checkBanned(strings.HasSuffix, s, Extensions)
}

var bannedHrefPrefixes = []string{
	"#",
	"mailto",
	"javascript",
}

func isBannedHref(s string) bool {
	return checkBanned(strings.HasPrefix, s, bannedHrefPrefixes)
}

func (p *Parser) ExtractAllUrls(baseUrl *net.URL, html []byte) ([]*net.URL, error) {
	hrefs := []*net.URL{}

	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(html))
	if err != nil {
		return hrefs, err
	}

	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		href, ok := s.Attr("href")
		if !ok {
			return
		}
		if isBannedExtension(href) || isBannedHref(href) {
			return
		}

		u, err := NewURL(baseUrl, href)
		if err != nil {
			panic(err)
		}

		hrefs = append(hrefs, u)
	})

	return hrefs, nil
}
