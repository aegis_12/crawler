package queue

import (
	"crawler/crawler/net"
	"log"
	"math/rand"
	"time"

	"sync"

	"strings"

	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

var SQSQueuePrefix = "crawler_"
var SQSMaxNumMessages = 5

func init() {
	rand.Seed(time.Now().Unix())
}

// ---- AWS SQS Message ----

type SQSMessage struct {
	message  *sqs.Message
	queue    *SQSQueue
	queueURL string

	u *net.URL
}

func NewSQSMessage(m *sqs.Message, queue *SQSQueue, queueURL string, u *net.URL) Message {
	return &SQSMessage{
		message:  m,
		queue:    queue,
		queueURL: queueURL,
		u:        u,
	}
}

func (m *SQSMessage) Ack() {
	m.queue.ack(*m.message.ReceiptHandle, m.queueURL)
}

func (m *SQSMessage) Url() *net.URL {
	return m.u
}

// ---- AWS SQS Queue ----

type SQSQueue struct {
	logger *log.Logger

	svc       *sqs.SQS
	queueURLS []string

	urlsCh      chan *Message
	pendingUrls []string

	sendMutex *sync.Mutex

	stopReceiveCh chan struct{}
	stopSendCh    chan struct{}
}

func NewSQSQueue(logger *log.Logger, sess *session.Session) (Queue, error) {
	svc := sqs.New(sess)

	d := &SQSQueue{
		logger:        logger,
		svc:           svc,
		queueURLS:     make([]string, 0),
		urlsCh:        make(chan *Message),
		pendingUrls:   make([]string, 0),
		sendMutex:     &sync.Mutex{},
		stopReceiveCh: make(chan struct{}),
		stopSendCh:    make(chan struct{}),
	}

	err := d.getValidQueues()
	if err != nil {
		return nil, err
	}

	return d, nil
}

func (s *SQSQueue) Close() {
	s.stopReceiveCh <- struct{}{}
	s.stopSendCh <- struct{}{}
}

func (s *SQSQueue) CreateQueue(name string) error {
	if !strings.HasPrefix(name, SQSQueuePrefix) {
		name = SQSQueuePrefix + name
	}

	_, err := s.svc.CreateQueue(&sqs.CreateQueueInput{
		QueueName: &name,
	})

	return err
}

func (s *SQSQueue) getValidQueues() error {
	list, err := s.svc.ListQueues(&sqs.ListQueuesInput{
		QueueNamePrefix: &SQSQueuePrefix,
	})
	if err != nil {
		return err
	}

	for _, u := range list.QueueUrls {
		s.queueURLS = append(s.queueURLS, *u)
	}

	return nil
}

func (s *SQSQueue) ListQueues() []string {
	return s.queueURLS
}

func (s *SQSQueue) getRandomQueue() string {
	return s.queueURLS[rand.Intn(len(s.queueURLS))]
}

func (s *SQSQueue) ack(receiptHandle, queueURL string) {
	_, err := s.svc.ChangeMessageVisibility(&sqs.ChangeMessageVisibilityInput{
		QueueUrl:      &queueURL,
		ReceiptHandle: &receiptHandle,
	})

	if err != nil {

	}
}

func (s *SQSQueue) Start() error {
	if len(s.ListQueues()) == 0 {
		return fmt.Errorf("No queues found")
	}

	go s.runReceive()
	go s.runSend()

	return nil
}

func (s *SQSQueue) Receive() chan *Message {
	return s.urlsCh
}

func (s *SQSQueue) runReceive() error {

	for {
		select {
		case <-s.stopReceiveCh:
			break
		default:
			queueURL := s.getRandomQueue()

			result, err := s.svc.ReceiveMessage(&sqs.ReceiveMessageInput{
				QueueUrl:            &queueURL,
				MaxNumberOfMessages: aws.Int64(int64(SQSMaxNumMessages)),
				VisibilityTimeout:   aws.Int64(10),
				WaitTimeSeconds:     aws.Int64(10),
			})
			if err != nil {
				continue
			}

			if len(result.Messages) == 0 {
				continue
			}

			for _, m := range result.Messages {
				u, err := net.NewURL(*m.Body)
				if err != nil {
					continue
				}

				message := NewSQSMessage(m, s, queueURL, u)
				s.urlsCh <- &message
			}
		}
	}

	return nil
}

func (s *SQSQueue) Send(urls []*net.URL) {
	plainurls := []string{}
	for _, i := range urls {
		plainurls = append(plainurls, (*i).String())
	}

	s.sendMutex.Lock()
	s.pendingUrls = append(s.pendingUrls, plainurls...)
	s.sendMutex.Unlock()
}

// Size of each batch request
const batchSize = 10

// Number of batch requests
const batchNum = 5

func (s *SQSQueue) runSend() {
	for {
		select {
		case <-s.stopSendCh:
			break
		case <-time.After(20 * time.Second):
			s.sendMutex.Lock()

			s.sendMutex.Unlock()
		}
	}
}
