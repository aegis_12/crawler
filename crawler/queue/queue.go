package queue

import "crawler/crawler/net"

type Message interface {
	Ack()
	Url() *net.URL
}

type Queue interface {
	Receive() chan *Message
	Send([]*net.URL)
	ListQueues() []string
	CreateQueue(name string) error
	Close()
}
