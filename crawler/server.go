package crawler

import (
	"crawler/crawler/cache"
	"crawler/crawler/net"
	"crawler/crawler/parser"
	"crawler/crawler/queue"
	"crawler/crawler/storage"
	"log"
)

type HttpResponse struct {
	Content []byte
	err     error
	msg     *queue.Message
}

type Server struct {
	logger *log.Logger
	stopCh chan struct{}

	fetch *net.Fetch

	// Storage
	storageSvc storage.Storage

	// Queue
	queueSvc queue.Queue

	// workers to concurrently download pages
	httpRequest  chan *queue.Message
	httpResponse chan *HttpResponse
	numWorkers   int

	// cache and parser
	urlCache  cache.Cache
	urlParser parser.Parser
}

func NewServer(logger *log.Logger, urlCache cache.Cache, urlParser parser.Parser, fetch *net.Fetch, storageSvc storage.Storage, queueSvc queue.Queue) (*Server, error) {
	s := &Server{
		logger:     logger,
		stopCh:     make(chan struct{}),
		fetch:      fetch,
		storageSvc: storageSvc,
		queueSvc:   queueSvc,
		urlCache:   urlCache,
		urlParser:  urlParser,
	}

	go s.run()

	return s, nil
}

func (s *Server) extractURLs(baseurl *net.URL, content []byte) ([]*net.URL, error) {
	validUrls := []*net.URL{}

	// parser
	allUrls, err := s.urlParser.ExtractUrls(baseurl, content)
	if err != nil {
		return validUrls, err
	}

	// cache
	for _, u := range allUrls {
		exists, err := s.urlCache.Get(u.String())
		if err != nil {
			continue
		}
		if exists {
			continue
		}

		err = s.urlCache.Set(u.String())
		if err != nil {
			continue
		}

		validUrls = append(validUrls, u)
	}

	return validUrls, nil
}

func (s *Server) worker(id int, jobs <-chan *queue.Message, results chan<- *HttpResponse) {
	for m := range jobs {
		html, err := s.fetch.Get((*m).Url().String())
		results <- &HttpResponse{html, err, m}
	}
}

func (s *Server) initializeWorkers() {
	for w := 1; w <= s.numWorkers; w++ {
		go s.worker(w, s.httpRequest, s.httpResponse)
	}
}

func (s *Server) run() {
	for {
		select {
		case m := <-s.queueSvc.Receive():
			s.httpRequest <- m

		case r := <-s.httpResponse:
			if r.err != nil {
				continue
			}

			urls, err := s.extractURLs((*r.msg).Url(), r.Content)
			if err != nil {
				continue
			}

			s.storageSvc.CheckAsync(urls)

		case urls := <-s.storageSvc.ReceiveAsync():
			s.queueSvc.Send(urls)
			s.storageSvc.SaveAsync(urls)

		case <-s.stopCh:
			close(s.httpRequest)
			break
		}
	}
}

func (s *Server) Shutdown() {
	// stop queue
	s.queueSvc.Close()

	// stop storage
	s.storageSvc.Close()

	// stop scheduler
	s.stopCh <- struct{}{}
}
