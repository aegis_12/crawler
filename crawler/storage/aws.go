package storage

import (
	"fmt"
	"log"
	"sync"

	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

const dynamoDBTable = "crawledurls"

type DynamoDBStorage struct {
	logger *log.Logger
	svc    *dynamodb.DynamoDB

	stopCh    chan struct{}
	checkedCh chan []string

	checkMutex     *sync.Mutex
	pendingChecked []string

	saveMutex    *sync.Mutex
	pendingSaved []string
}

func NewDynamoDBStorage(logger *log.Logger, sess *session.Session) (Storage, error) {
	svc := dynamodb.New(sess)

	d := &DynamoDBStorage{
		logger:         logger,
		svc:            svc,
		pendingChecked: make([]string, 0),
	}

	err := d.createDBIfNotFound()
	if err != nil {
		return nil, err
	}

	return d, nil
}

func (d *DynamoDBStorage) createDBIfNotFound() error {

	// check if table exists
	resp, err := d.svc.DescribeTable(&dynamodb.DescribeTableInput{
		TableName: aws.String(dynamoDBTable),
	})

	fmt.Println(resp)
	fmt.Println(err)

	// create table
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Url"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("Checked"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Url"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Checked"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
		TableName: aws.String(dynamoDBTable),
	}

	_, err := d.svc.CreateTable(input)
	if err != nil {
		return err
	}

	err = d.svc.WaitUntilTableExists(&dynamodb.DescribeTableInput{
		TableName: aws.String(dynamoDBTable),
	})
	if err != nil {
		return err
	}

	return nil
}

func (d *DynamoDBStorage) CheckAsync(urls []string) {
	d.checkMutex.Lock()
	d.pendingChecked = append(d.pendingChecked, urls...)
	d.checkMutex.Unlock()
}

func (d *DynamoDBStorage) runCheckAsync() {
	for {
		select {
		case <-d.stopCh:
			break
		case <-time.After(10 * time.Second):
			// check
		}
	}
}

func (d *DynamoDBStorage) runSaveAsync() {
	for {
		select {
		case <-d.stopCh:
			break
		case <-time.After(10 * time.Second):
			// save to db
		}
	}
}

func (d *DynamoDBStorage) SaveAsync(urls []string) {
	d.saveMutex.Lock()
	d.pendingSaved = append(d.pendingSaved, urls...)
	d.saveMutex.Unlock()
}

func (d *DynamoDBStorage) ReceiveAsync() chan []string {
	return d.checkedCh
}

func (d *DynamoDBStorage) Close() {
	d.stopCh <- struct{}{}
	d.stopCh <- struct{}{}
}
