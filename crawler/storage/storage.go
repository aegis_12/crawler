package storage

import "crawler/crawler/net"

type Storage interface {
	CheckAsync(urls []*net.URL)
	SaveAsync(urls []*net.URL)
	ReceiveAsync() chan []*net.URL
	Close()
}
